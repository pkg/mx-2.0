mx-2.0 (1.99.5+20130417gita614f7-0co14) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to target

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 17 Feb 2021 21:22:48 +0000

mx-2.0 (1.99.5+20130417gita614f7-0co13) 15.09; urgency=medium

  * debian/patches/kinetic-scroll-view-dont-propagate-press-events.patch:
    Added. (Phab: D763)

 -- Jonny Lamb <jonny.lamb@collabora.co.uk>  Wed, 21 Oct 2015 15:06:46 +0200

mx-2.0 (1.99.5+20130417gita614f7-0co12) 15.09; urgency=medium

  * debian/patches/0001-kinetic-scroll-view-Handle-ClutterTimeline-stopped-n.patch
  * debian/patches/0003-kinetic-scroll-view-Add-more-debug-output.patch
     + Updated. Fix potential NULL unref and drop an unrelated change.
       (Apertis: #401) (Phab: D501, D503)

 -- Philip Withnall <philip.withnall@collabora.co.uk>  Tue, 22 Sep 2015 11:00:00 +0100

mx-2.0 (1.99.5+20130417gita614f7-0co11) 15.09; urgency=medium

  * debian/patches/0001-kinetic-scroll-view-Handle-ClutterTimeline-stopped-n.patch
  * debian/patches/0002-kinetic-scroll-view-Grow-the-motion-buffer-over-time.patch
  * debian/patches/0003-kinetic-scroll-view-Add-more-debug-output.patch
  * debian/patches/0004-kinetic-scroll-view-Replace-motion-buffer-with-a-mov.patch
  * debian/patches/0005-kinetic-scroll-view-Use-gint64-for-timestamps-rather.patch
  * debian/patches/0006-kinetic-scroll-view-Include-timestamps-in-debug-mess.patch
  * debian/patches/0007-stylable-Disable-propagating-style-changed-signals-t.patch
  * debian/patches/0008-settings-Enable-MxSettings-touch-mode-by-default.patch
     + Added. Series of fixes for kinetic scrolling in MxKineticScrollView.
       (Apertis: #401) (Phab: D501-D508)
  * debian/rules: Fix moving background PNG files if the destination already
    exists.

 -- Philip Withnall <philip.withnall@collabora.co.uk>  Mon, 21 Sep 2015 18:47:00 +0100

mx-2.0 (1.99.5+20130417gita614f7-0co10) 15.09; urgency=medium

  * debian/patches/0002-Add-MxKineticScrollView-test.patch:
    + Updated. Throttle motion events, disable snapping to page, and set
      scroll policy to vertical. (Apertis: #388) (Phab: D462)
  * debian/patches/0001-mx-style-Do-not-call-g_file_test-NULL.patch:
    + Added. Avoid calling access(NULL). (Phab: D477)

 -- Philip Withnall <philip.withnall@collabora.co.uk>  Tue, 15 Sep 2015 13:11:00 +0100

mx-2.0 (1.99.5+20130417gita614f7-0co9) 15.09; urgency=medium

  * debian/patches/0001-scroll-bar-handle-touch-events.patch:
    + Added. Listen to touch events in MxScrollBar. (Apertis: #422)
      (Phab: T382)

 -- Jonny Lamb <jonny.lamb@collabora.co.uk>  Mon, 14 Sep 2015 17:16:03 +0200

mx-2.0 (1.99.5+20130417gita614f7-0co8) 15.09; urgency=medium

  * d/p/dont-use-xsettings.patch:
    + Dropped
  * debian/rules: Build with the wayland backend
  * debian/patches/no-backend-specifics.patch:
    + Added. Disable support resizable window UI furniture as this is the only
      bit in the MX wayland "backend" which makes it non-generic. Given it's
      not used on Apertis simply disabling ensures MX works on both X11 and
      Wayland

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Tue, 30 Jun 2015 15:43:06 +0200

mx-2.0 (1.99.5+20130417gita614f7-0co7) 15.09; urgency=medium

  * d/p/dont-use-xsettings.patch:
    + Added. Don't use X settings, they're not used and make the settings
      implementation window system specific.

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Tue, 23 Jun 2015 09:53:14 +0200

mx-2.0 (1.99.5+20130417gita614f7-0co6rb1co1) 15.03; urgency=low

  * Applied 0001-Support-rollover-on-adjustments.patch: It is required to 
    fix some issues in the roller. (SAC: #65)

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Wed, 11 Mar 2015 02:47:24 +0800

mx-2.0 (1.99.5+20130417gita614f7-0co6rb1) jasmine; urgency=low

  * New patch created
     debian/patches:default_css_for_progress_bar.patch applied (CHAI: #2924)

 -- rbei-b+i <rbei-b+i@in.bosch.com>  Fri, 12 Sep 2014 16:22:29 +0530

mx-2.0 (1.99.5+20130417gita614f7-0co6) jasmine; urgency=low

  * Replace two new images for adjust the look and feel. (CHAI: #2924)

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Fri, 12 Sep 2014 18:15:08 +0800

mx-2.0 (1.99.5+20130417gita614f7-0co5) earlgrey; urgency=low

  * debian/patches/0001-kinetic-scroll-view-use-mx_adjustment_interpolate-in.patch:
    - Drop patch due to regressions. (CHAI: #949) 

 -- Hector Oron <hector.oron@collabora.co.uk>  Wed, 07 Aug 2013 14:32:06 +0200

mx-2.0 (1.99.5+20130417gita614f7-0co4) earlgrey; urgency=low

  * debian/patches/0003-Use-clamp-value.patch:
    - Add patch from 1.4.6 to improve MxKineticScrollView behavior (CHAI: #799)

 -- Rodrigo Moya <rodrigo.moya@collabora.co.uk>  Wed, 05 Jun 2013 10:41:30 +0200

mx-2.0 (1.99.5+20130417gita614f7-0co3) earlgrey; urgency=low

  * debian/patches/0002-Add-MxKineticScrollView-test.patch:
    - Add test for MxKineticScrollView
  * debian/rules:
    - Add --enable-tests flag so that tests are built
  * debian/control:
  * debian/libmx-2.0-0-tests.install:
    - Add -tests package to hold the tests

 -- Rodrigo Moya <rodrigo.moya@collabora.co.uk>  Wed, 24 Apr 2013 09:57:03 +0200

mx-2.0 (1.99.5+20130417gita614f7-0co2) earlgrey; urgency=low

  * debian/patches/0001-kinetic-scroll-view-use-mx_adjustment_interpolate-in.patch:
    - Add new patch for smooth panning in MxKineticScrollView

 -- Rodrigo Moya <rodrigo.moya@icollabora.co.uk>  Fri, 19 Apr 2013 14:03:55 +0200

mx-2.0 (1.99.5+20130417gita614f7-0co1) earlgrey; urgency=low

  * New upstream release.

 -- Hector Oron <hector.oron@collabora.co.uk>  Wed, 17 Apr 2013 17:10:43 +0200

mx-2.0 (1.99.4+20130307gitcf5edd-0co2) earlgrey; urgency=low

  * d/p/fix-broken-sgml-mx-docs.patch:
    - Unbreak build disabling broken sgml. 

 -- Hector Oron <hector.oron@collabora.co.uk>  Mon, 01 Apr 2013 20:19:47 +0200

mx-2.0 (1.99.4+20130307gitcf5edd-0co1) darjeeling; urgency=low

  * Rename upstream tarball. 
  * Update symbols.

 -- Hector Oron <hector.oron@collabora.co.uk>  Thu, 28 Mar 2013 15:32:12 +0000

mx-2.0 (2.0.0-0co2) darjeeling; urgency=low

  * Rename doc package to libmx-2.0-doc.
  * Allow installing documentation in parallel with libmx-doc.

 -- Hector Oron <hector.oron@collabora.co.uk>  Thu, 14 Mar 2013 13:26:44 +0000

mx-2.0 (2.0.0-0co1) darjeeling; urgency=low

  * New upstream git snapshot release. (BUG: #1444)
  * debian/control:
    - Drop libmx-bin binary package.
    - Drop libmx-common binary package.
    - Rename binary package name to libmx-2.0-0, libmx-2.0-0-dbg,
      gir1.2-mx-2.0, libmx-2.0-0, libmx-2.0-0-dev, libmx-2.0-0-doc.
    - drop Replaces and Breaks.
  * drop patches.
  * debian/libmx-2.0-0-doc.install: drop mx-gtk html.
  * debian/libmx-2.0-0-doc.links: drop mx-gtk links.
  * drop mx-create-image-cache.sgml
  * debian/rules: drop mx-create-image-cache docs.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Thu, 07 Mar 2013 20:08:37 +0800

mx (1.4.6-1ubuntu2co1) ceylon; urgency=low

  * Merge exist changes from ceylon:

  [ Hector Oron ]
  * Ensure building with clutter version that uses the new cogl ABI.

  [ Gustavo Noronha Silva ]
  * Build without-clutter-imcontext and without-clutter-gesture. (Bug#207)

  [ Derek Foreman ]
  * kinetic-scroll-view: Add a new automatic scroll policy. (BUG: #651)

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Mon, 29 Oct 2012 17:32:37 +0800

mx (1.4.6-1ubuntu2) quantal; urgency=low

  * debian/control: Drop clutter-imcontext dependency from -dev package, too.
    (See LP #1003160)

 -- Martin Pitt <martin.pitt@ubuntu.com>  Thu, 31 May 2012 06:16:49 +0200

mx (1.4.6-1ubuntu1) quantal; urgency=low

  * debian/control.in:
    - Don't build-depend on libclutter-imcontext-0.1-dev or
      libcluttergesture-dev; they aren't in main and appear to have been
      abandoned by their developers

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sat, 26 May 2012 18:24:33 -0400

mx (1.4.6-1) unstable; urgency=low

  * New upstream release

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Wed, 16 May 2012 20:41:13 +0800

mx (1.4.5-1) unstable; urgency=low

  * New upstream release

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 01 Apr 2012 02:09:54 +0800

mx (1.4.3-2) unstable; urgency=low

  * Fix typelib files installed in wrong directory (Closes: #664818)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Wed, 21 Mar 2012 10:07:56 +0800

mx (1.4.3-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.3: nothing needs to be changed.
  * Update debian/copyright to format 1.0
  * debian/control: gir1.2-mx-1.0 Section: libs -> introspection
  * Multi-Arch support
    - Bump debhelper version to 9
    - Split libmx-bin and libmx-common from shared library package

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 18 Mar 2012 02:17:30 +0800

mx (1.4.2-1) unstable; urgency=low

  * New upstream release
    - Use dynamic memory allocation for paths (Closes: #648187)
  * Change Build-Depends gir1.2-json-glib-1.0 -> gir1.2-json-1.0

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sat, 03 Mar 2012 11:19:51 +0800

mx (1.4.1-1) unstable; urgency=low

  * New upstream release
  * Update debian/copyright to latest DEP-5 format

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 20 Nov 2011 14:35:12 +0800

mx (1.3.1-1) unstable; urgency=low

  * New upstream release

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 01 Sep 2011 22:55:23 +0800

mx (1.3.0-1) unstable; urgency=low

  * New upstream release

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 04 Aug 2011 03:38:29 +0800

mx (1.2.0-1) unstable; urgency=low

  * New upstream release
  * Provide .symbols file (Closes: #631523)
  * Add mx-gtk doc in libmx-doc

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 28 Jun 2011 01:28:46 +0800

mx (1.1.12-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.2: nothing needs to be changed.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 22 May 2011 23:24:18 +0800

mx (1.1.10-2) unstable; urgency=low

  * Remove gir-repository-dev from Build-Depends

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Wed, 13 Apr 2011 03:14:15 +0800

mx (1.1.10-1) unstable; urgency=low

  * New upstream release
  * Remove debian/patches/02_fix_implicit_mx.types.patch: upstreamed

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 29 Mar 2011 16:38:09 +0800

mx (1.1.8-3) experimental; urgency=low

  * Fix FTBFS: Add build depends on libgirepository1.0-dev (>= 0.10.0)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 07 Mar 2011 03:19:54 +0800

mx (1.1.8-2) experimental; urgency=low

  * Add build depends on gobject-introspection (>= 0.10.0) to fix FTBFS
    (Closes: #616555)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 07 Mar 2011 01:49:22 +0800

mx (1.1.8-1) experimental; urgency=low

  * New upstream release.
    - Fixed FTBFS with gold or ld --no-add-needed (Closes: #615755)
  * Change upstream to clutter (Closes: #615534)
    - Update debian/control, debian/watch, debian/copyright
  * Add gir1.2 packages.
  * Use dh-autoreconf in debian/rule
  * Bump Standards-Version to 3.9.1: nothing needs to be changed.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 28 Feb 2011 23:14:10 +0800

mx (1.0.2-1) unstable; urgency=low

  * New upstream release
  * Upstream soname bumped: 1.0-0 -> 1.0-2. Renaming packages.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 30 May 2010 21:32:55 +0800

mx (1.0.0-1) unstable; urgency=low

  * New upstream release

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Wed, 19 May 2010 01:16:10 +0800

mx (0.99.5-1) unstable; urgency=low

  * New upstream release (Closes: #577302)
  * Change to DebSrc 3.0 format
  * Remove 05_backport_clutter-1.0.patch: In upstream.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sat, 17 Apr 2010 15:07:57 +0800

mx (0.7.1-1) unstable; urgency=low

  * New upstream release
  * debian/patches/05_backport_clutter-1.0.patch:
    - Backport to clutter 1.0

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Fri, 26 Feb 2010 03:20:28 +0800

mx (0.6.0-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Add Build-Depends on libcluttergesture-dev
    - Bump Standards-Version to 3.8.4: Nothing need to be changed
    - Add Build-Depends on libstartup-notification0-dev
    - Add Build-Depends on libdbus-glib-1-dev

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 09 Feb 2010 23:47:55 +0800

mx (0.4.0-1) unstable; urgency=low

  * New upstream release

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 25 Jan 2010 03:15:30 +0800

mx (0.3.0-1) unstable; urgency=low

  * Initial release (Closes: #563652)
  * Refresh debian/copyright
  * Remove installing .la files

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 12 Jan 2010 15:17:51 +0800
