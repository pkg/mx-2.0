From e5b0da33f550b2ae9daf07fb4bd76ab0f2452deb Mon Sep 17 00:00:00 2001
From: Gustavo Noronha Silva <gustavo.noronha@collabora.co.uk>
Date: Wed, 25 Feb 2015 16:47:51 -0300
Subject: [PATCH] Support rollover on adjustments

---
 mx/mx-adjustment.c      | 102 +++++++++++++++++++++++++++++++++++-
 mx/mx-adjustment.h      |   4 ++
 tests/Makefile.am       |   3 ++
 tests/test-adjustment.c | 134 ++++++++++++++++++++++++++++++++++++++++++++++++
 4 files changed, 242 insertions(+), 1 deletion(-)
 create mode 100644 tests/test-adjustment.c

diff --git a/mx/mx-adjustment.c b/mx/mx-adjustment.c
index 0685e80..3a05898 100644
--- a/mx/mx-adjustment.c
+++ b/mx/mx-adjustment.c
@@ -55,6 +55,8 @@ struct _MxAdjustmentPrivate
   guint clamp_value     : 1;
   guint elastic         : 1;
 
+  gboolean rollover     : 1;
+
   gdouble  lower;
   gdouble  upper;
   gdouble  value;
@@ -90,6 +92,7 @@ enum
 
   PROP_ELASTIC,
   PROP_CLAMP_VALUE,
+  PROP_ROLLOVER,
 };
 
 enum
@@ -180,6 +183,10 @@ mx_adjustment_get_property (GObject    *gobject,
       g_value_set_boolean (value, priv->clamp_value);
       break;
 
+    case PROP_ROLLOVER:
+      g_value_set_boolean (value, priv->rollover);
+      break;
+
     default:
       G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
       break;
@@ -228,6 +235,10 @@ mx_adjustment_set_property (GObject      *gobject,
       mx_adjustment_set_clamp_value (adj, g_value_get_boolean (value));
       break;
 
+    case PROP_ROLLOVER:
+      mx_adjustment_set_rollover (adj, g_value_get_boolean (value));
+      break;
+
     default:
       G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
       break;
@@ -371,6 +382,23 @@ mx_adjustment_class_init (MxAdjustmentClass *klass)
                                                          MX_PARAM_READWRITE));
 
   /**
+   * MxAdjustment::rollover:
+   *
+   * Whether the adjustment should restart from the other end
+   * when it hits a boundary. Implies MxAdjustment:clamp-value
+   * and MxAdjustment:elastic to both be %FALSE.
+   */
+  g_object_class_install_property (object_class,
+                                   PROP_ROLLOVER,
+                                   g_param_spec_boolean ("rollover",
+                                                         "Rollover",
+                                                         "Cause adjustment to restart "
+                                                         "from the other end when it "
+                                                         "hits a boundary.",
+                                                         FALSE,
+                                                         MX_PARAM_READWRITE));
+
+  /**
    * MxAdjustment::changed-immediate:
    *
    * Emitted when any of the adjustment values have changed
@@ -420,7 +448,8 @@ mx_adjustment_init (MxAdjustment *self)
   self->priv = ADJUSTMENT_PRIVATE (self);
 
   self->priv->is_constructing = TRUE;
-  self->priv->clamp_value = TRUE;
+  self->priv->clamp_value = FALSE;
+  self->priv->rollover = FALSE;
 }
 
 /**
@@ -594,6 +623,18 @@ mx_adjustment_set_value (MxAdjustment *adjustment,
 
   if (priv->value != value)
     {
+      /* Please note that rollover for interpolation is done at
+       * interpolation_new_frame_cb, since it requires mangling
+       * of there parameters of the interpolation.
+       */
+      if (priv->rollover)
+        {
+          if (value > priv->upper)
+            value = value - priv->upper;
+          else if (value < 0)
+            value = priv->upper + value; // Effectively a subtraction.
+        }
+
       stop_interpolation (adjustment);
 
       priv->value = value;
@@ -1057,6 +1098,27 @@ interpolation_new_frame_cb (ClutterTimeline *timeline,
     (priv->new_position - priv->old_position) *
     clutter_timeline_get_progress (priv->interpolation);
 
+  if (priv->rollover &&
+      (new_value > priv->upper || new_value < 0))
+    {
+      gdouble to_go;
+      gdouble updated_new_position;
+
+      to_go = priv->new_position - new_value;
+
+      if (new_value > priv->upper)
+        {
+          to_go = fabs (to_go);
+          new_value = new_value - priv->upper;
+        }
+      else
+        new_value = priv->upper + new_value;  // Effectively a subtraction.
+
+      updated_new_position = new_value + to_go;
+      priv->old_position =  updated_new_position - (priv->new_position - priv->old_position);
+      priv->new_position = updated_new_position;
+    }
+
   priv->interpolation = NULL;
   mx_adjustment_set_value (adjustment, new_value);
   priv->interpolation = timeline;
@@ -1260,3 +1322,41 @@ mx_adjustment_set_clamp_value (MxAdjustment *adjustment,
   adjustment->priv->clamp_value = clamp;
 }
 
+/**
+ * mx_adjustment_get_rollover:
+ * @adjustment: A #MxAdjustment
+ *
+ * Get the value of the #MxAdjustment:rollover property.
+ *
+ * Returns: the current value of the "rollover" property.
+ *
+ * Since: 2.2
+ */
+gboolean
+mx_adjustment_get_rollover (MxAdjustment *adjustment)
+{
+  return adjustment->priv->rollover;
+}
+
+/**
+ * mx_adjustment_set_rollover:
+ * @adjustment: A #MxAdjustment
+ * @rollover: a #gboolean
+ *
+ * Set the value of the #MxAdjustment:rollover property.
+ *
+ * Since: 2.2
+ */
+void
+mx_adjustment_set_rollover (MxAdjustment *adjustment,
+                            gboolean      rollover)
+{
+  if (rollover)
+    {
+      mx_adjustment_set_clamp_value (adjustment, FALSE);
+      mx_adjustment_set_elastic (adjustment, FALSE);
+    }
+
+  adjustment->priv->rollover = rollover;
+}
+
diff --git a/mx/mx-adjustment.h b/mx/mx-adjustment.h
index f828e86..1d854ea 100644
--- a/mx/mx-adjustment.h
+++ b/mx/mx-adjustment.h
@@ -149,6 +149,10 @@ gboolean      mx_adjustment_get_clamp_value (MxAdjustment *adjustment);
 void          mx_adjustment_set_clamp_value (MxAdjustment *adjustment,
                                              gboolean      clamp);
 
+gboolean      mx_adjustment_get_rollover (MxAdjustment *adjustment);
+void          mx_adjustment_set_rollover (MxAdjustment *adjustment,
+                                          gboolean      rollover);
+
 G_END_DECLS
 
 #endif /* __MX_ADJUSTMENT_H__ */
diff --git a/tests/Makefile.am b/tests/Makefile.am
index 580c26e..4e107e1 100644
--- a/tests/Makefile.am
+++ b/tests/Makefile.am
@@ -13,6 +13,7 @@ tests_PROGRAMS = test-kinetic-scroll-view
 test_kinetic_scroll_view_SOURCES = test-kinetic-scroll-view.c
 
 noinst_PROGRAMS = 			\
+	test-adjustment			\
 	test-draggable			\
 	test-droppable			\
 	test-window 			\
@@ -20,6 +21,8 @@ noinst_PROGRAMS = 			\
 	test-containers
 	$(NULL)
 
+test_adjustment_SOURCES = test-adjustment.c
+
 test_widgets_SOURCES = test-widgets.c
 test_containers_SOURCES = test-containers.c
 
diff --git a/tests/test-adjustment.c b/tests/test-adjustment.c
new file mode 100644
index 0000000..6080ed0
--- /dev/null
+++ b/tests/test-adjustment.c
@@ -0,0 +1,134 @@
+/*
+ * Copyright 2015 Collabora Ltd.
+ *
+ * This program is free software; you can redistribute it and/or modify it
+ * under the terms and conditions of the GNU Lesser General Public License,
+ * version 2.1, as published by the Free Software Foundation.
+ *
+ * This program is distributed in the hope it will be useful, but WITHOUT ANY
+ * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
+ * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
+ * more details.
+ *
+ * You should have received a copy of the GNU Lesser General Public License
+ * along with this program; if not, write to the Free Software Foundation,
+ * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
+ * Boston, MA 02111-1307, USA.
+ *
+ */
+
+#include <mx/mx.h>
+
+#define PASS() g_print("%s: PASS\n", __func__);
+
+static void
+test_upper_rollover_cb (MxAdjustment *adjustment, GMainLoop *main_loop)
+{
+  g_assert_cmpfloat (mx_adjustment_get_value (adjustment), ==, 50.);
+  g_application_release (g_application_get_default ());
+  PASS();
+}
+
+static void
+test_lower_rollover_cb (MxAdjustment *adjustment, GMainLoop *main_loop)
+{
+  g_assert_cmpfloat (mx_adjustment_get_value (adjustment), ==, 603.);
+  g_application_release (g_application_get_default ());
+  PASS();
+}
+
+static void
+test_rollover_twice_cb (MxAdjustment *adjustment, GMainLoop *main_loop)
+{
+  static gboolean been_here = FALSE;
+
+  if (been_here)
+    {
+      g_assert_cmpfloat (mx_adjustment_get_value (adjustment), ==, 600.);
+      g_application_release (g_application_get_default ());
+      PASS();
+      return;
+    }
+
+  been_here = TRUE;
+
+  g_assert_cmpfloat (mx_adjustment_get_value (adjustment), ==, 50.);
+  mx_adjustment_interpolate (adjustment, -53., 1000, CLUTTER_EASE_IN_CUBIC);
+}
+
+static MxAdjustment*
+create_adjustment_for_rollover ()
+{
+  MxAdjustment *adjustment;
+
+  adjustment = mx_adjustment_new_with_values (0., 0., 653., 5., 20., 30.);
+  g_object_set (G_OBJECT (adjustment),
+                "clamp-value", FALSE,
+                "elastic", FALSE,
+                "rollover", TRUE,
+                NULL);
+
+  return adjustment;
+}
+
+static void
+startup_cb (MxApplication *application)
+{
+  MxWindow *window;
+  MxAdjustment *adjustment;
+
+  window = mx_application_create_window (application, "Test Adjustment");
+  mx_window_show (window);
+
+  adjustment = create_adjustment_for_rollover ();
+
+  /* First some non-interpolated rollover tests. */
+  mx_adjustment_set_value (adjustment, 703.);
+  g_assert_cmpfloat (mx_adjustment_get_value (adjustment), ==, 50.);
+
+  g_print("non-interpolated rollover: PASS\n");
+
+  /* Test upper rollover. */
+  g_signal_connect (adjustment, "interpolation-completed",
+                    G_CALLBACK (test_upper_rollover_cb), NULL);
+
+  mx_adjustment_interpolate (adjustment, 703., 2000, CLUTTER_EASE_IN_CUBIC);
+
+  g_application_hold (G_APPLICATION (application));
+
+  /* Test lower rollver. */
+  adjustment = create_adjustment_for_rollover ();
+
+
+  g_signal_connect (adjustment, "interpolation-completed",
+                    G_CALLBACK (test_lower_rollover_cb), NULL);
+
+  mx_adjustment_interpolate (adjustment, -50., 1500, CLUTTER_EASE_IN_CUBIC);
+
+  g_application_hold (G_APPLICATION (application));
+
+  /* Test rolling over twice. */
+  adjustment = create_adjustment_for_rollover ();
+
+
+  g_signal_connect (adjustment, "interpolation-completed",
+                    G_CALLBACK (test_rollover_twice_cb), NULL);
+
+  mx_adjustment_interpolate (adjustment, 703., 1000, CLUTTER_EASE_IN_CUBIC);
+
+  /* No hold on the last test since we want the MxWindow's one to be released. */
+}
+
+int
+main (int argc, char *argv[])
+{
+  MxApplication *application;
+
+  application = mx_application_new ("org.clutter-project.Mx.TestMxAdjustment", 0);
+  g_signal_connect_after (application, "startup", G_CALLBACK (startup_cb), NULL);
+
+  /* run the application */
+  g_application_run (G_APPLICATION (application), argc, argv);
+
+  return 0;
+}
-- 
2.1.0

